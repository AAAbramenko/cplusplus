#include <string>
#include <iostream>

#include "database.h"
#include "sensor.h"
#include "utils.h"

using namespace std;

void testUtils() {
    puts("Test utils:");
    puts("Test cstrToTime:");
    time_t t = cstrToTime("2008-09-10 11:12:13");
    print(t);
    printBlankLine();

    puts("Test timeToStr:");
    print(timeToStr(&t));
    printBlankLine();

    puts("Test getRndFromRange:");
    print(getRndFromRange(9, 10));
    print(getRndFromRange(90, 100));
    print(getRndFromRange(900, 1000));
    print(getRndFromRange(9000, 10000));
    printBlankLine();
}

void testSensor() {
    puts("Test Sensor:");
    Sensor sensor = Sensor(3, SensorType::temp, "Model", 100.0, 200.0, "2008-09-10 11:12:13");
    sensor.printData();
    print(sensor.getRandomVal());
    printBlankLine();
}

void testDatabase() 
{
    puts("Test Database:");
    const string filename = "test_data.db";
    Database db = Database(filename);
    cout << db.getFilename() << endl;
    cout << db.open() << endl;

    string sql = "DROP TABLE IF EXISTS TEST_TABLE";
    string mes;
    cout << db.exec(sql, nullptr, mes) << endl;

    sql = "CREATE TABLE TEST_TABLE("\
    "KEY INT PRIMARY KEY NOT NULL,"\
    "VAL INT)";
    cout << db.exec(sql, nullptr, mes) << endl;

    sql = "INSERT INTO TEST_TABLE VALUES"\
    "(101, 303)";
    cout << db.exec(sql, nullptr, mes) << endl; 

    sql = "SELECT KEY, VAL FROM TEST_TABLE";
    auto selectCallback = [](void *data, int argc, char **argv, char **azColName)
    {
        int key = stol(argv[0]);
        int val = stoi(argv[1]);
        cout << key << " " << val << endl;
        return 0;
    };
    cout << db.exec(sql, selectCallback, mes) << endl;
    
    cout << db.close() << endl;
}


int main(int argc, char* argv[]) {
    testUtils();
    testSensor();
    testDatabase();
}