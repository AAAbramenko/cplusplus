#include <string>
#include <iostream>

#include "database.h"
#include "lib/sqlite3/sqlite3.h"

using namespace std;

Database::Database(const string& fname)
{
    filename = fname;
}

const string& Database::getFilename() const 
{
    return filename;
}

bool Database::open()
{
    int result = sqlite3_open(filename.c_str(), &db);
    return result == SQLITE_OK;
}

bool Database::close()
{
    int result = sqlite3_close(db);
    return result == SQLITE_OK;
}

bool Database::exec(const string& sql, execSQLcallback callback, string& mes)
{
    char* errMsg = 0;
    int result = sqlite3_exec(db, sql.c_str(), callback, 0, &errMsg);
    if (errMsg) {
        mes = errMsg;
    }
    sqlite3_free(errMsg);
    return result == SQLITE_OK;
}