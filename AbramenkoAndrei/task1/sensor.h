#ifndef CPLUSPLUS_ABRAMENKOANDREI_TASK1_SENSOR_H_
#define CPLUSPLUS_ABRAMENKOANDREI_TASK1_SENSOR_H_
#include <string>

using namespace std;

enum SensorType {temp, humid};

struct Sensor
{
    unsigned int sens_id;
    SensorType sens_type;
    string sens_model;
    double sens_longitude;
    double sens_latitude;
    time_t sens_instTime;
    const char* const SENSOR_TYPES[2] = {"temp", "humid"};

    Sensor(
        unsigned int id,
        const SensorType type,
        const string& model,
        double longitude,
        double latitude,
        const char* instTime);

    void printData() const;

    short int getRandomVal() const;

    // From -60 to 60 Celsius degrees
    static short int getRndTemp();

    // From 0 to 100 percents
    static short int getRndHumidity();
};

struct SensorStat
{
    short int minVal;
    short int maxVal;
    double sum = 0;
    unsigned long int numOfVals = 0;

    void addVal(short int val);

    void printStat() const;

    // TODO: review to remove and reset all stat
    // from printStat()
    void resetStat();
};

#endif