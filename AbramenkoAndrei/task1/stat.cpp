#include <iostream>
#include <cstdlib>
#include <string>

#include "database.h"
#include "general_data.h"
#include "utils.h"

#define CHECK_FOR_QUIT if(input[0] == 'Q' || input[0] == 'q')

using namespace std;

static SensorStat stat = SensorStat();

static bool getSensorStat(unsigned int id, time_t startTime, time_t endTime);

int main(int argc, char* argv[])
{
    puts("List of sensors:");
    for (Sensor sensor : SENSORS) sensor.printData();
    printBlankLine();

    char input[20];
    unsigned int id;
    time_t startTime;
    time_t endTime;
    while(true)
    {
        puts("Enter sensor's id (or Q to quit):");
        cin.getline(input, 10); // TODO: validate input integer
        CHECK_FOR_QUIT break;
        id = atoi(input);

        puts("Enter start time (or Q to quit):");
        cin.getline(input, 20); // TODO: validate input time string
        CHECK_FOR_QUIT break;
        startTime = cstrToTime(input);

        puts("Enter end time (or Q to quit):");
        cin.getline(input, 20); // TODO: validate input time string
        CHECK_FOR_QUIT break;
        endTime = cstrToTime(input);

        if(!getSensorStat(id, startTime, endTime))
        {
            return EXIT_FAILURE;
        }

        printBlankLine();
    }
    return EXIT_SUCCESS;
}

static int putValToStat(void *data, int argc, char **argv, char **azColName) {
    time_t t = stol(argv[0]);
    int val = stoi(argv[1]);
    cout << timeToStr(&t) << ": " << val << endl;
    stat.addVal(val);
    return 0;
}

bool getSensorStat(unsigned int id, time_t startTime, time_t endTime)
{
    // Open database
    Database db = Database(FILENAME);
    if(!db.open())
    {
        puts("Fail open database");
        return false;
    }

    // Reading values and collect statistics
    stat.resetStat();
    printBlankLine();
    string sql = "SELECT TIME, ID" + to_string(id) + " FROM SENSORS_VALS ";
    sql += "WHERE TIME BETWEEN " + to_string(startTime) + " AND " + to_string(endTime) + ';';
    string message;
    if (!db.exec(sql, putValToStat, message))
    {
        puts("Fail reading values from database:");
        cout << message << endl;
        return false;
    }

    // Close connection to database
    if(!db.close())
    {
        puts("Fail close database");
        return false;
    }

    // Print statistics
    stat.printStat();
    return true;
}