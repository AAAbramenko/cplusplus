#ifndef CPLUSPLUS_ABRAMENKOANDREI_TASK1_UTILS_H_
#define CPLUSPLUS_ABRAMENKOANDREI_TASK1_UTILS_H_
#include <ctime>
#include <iostream>
#include <string>

using namespace std;

// Given string must be like "yyyy-mm-dd HH:MM:SS"
time_t cstrToTime(const char*);

// Returned string format like "yyyy-mm-dd HH:MM:SS"
string timeToStr(const time_t*);

// start and end values are included in random range
template<typename T> T getRndFromRange(T, T);

void printBlankLine();

template<typename T> void print(const T &val);

template<typename T> void print(const T &val) {
    cout << val << endl;
}

template<typename T> T getRndFromRange(T start, T end) {
    static time_t seed = 0;
    if (!seed) {
        seed = time(nullptr);
        srand(seed);
    }
    return rand() % (end - start + 1) + start;
}

#endif