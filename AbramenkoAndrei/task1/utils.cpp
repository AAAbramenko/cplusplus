#include "utils.h"
#include <ctime>
#include <iostream>
#include <string>

using namespace std;

time_t cstrToTime(const char* cstr) {
    struct tm t;
    int yy, month, dd, hh, mm, ss;
    // TODO: return EOF when fails
    sscanf(cstr, "%d-%d-%d %d:%d:%d", &yy, &month, &dd, &hh, &mm, &ss);
    t.tm_year = yy - 1900;
    t.tm_mon = month - 1;
    t.tm_mday = dd;
    t.tm_hour = hh;
    t.tm_min = mm;
    t.tm_sec = ss;
    t.tm_isdst = -1;
    return mktime(&t);
}

string timeToStr(const time_t* t) {
    char chars[20];
    struct tm* tm = localtime(t);
    strftime(chars, 20, "%Y-%m-%d %X", tm);
    return string(chars);
}

void printBlankLine() {
    cout << endl;
}